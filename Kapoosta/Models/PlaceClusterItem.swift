//
//  PlaceClusterItem.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 13/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import GoogleMapsUtils

class PlaceClusterItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var place: Place
    
    init(place: Place) {
        self.place = place
        self.position = place.position
    }
}
