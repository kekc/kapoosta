//
//  Place.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 24/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import SwiftyJSON
import GoogleMaps
import GoogleMapsUtils
import MapKit
struct Place: Codable {
    
    var id: Int
    var categoryIds: [Int]
    var title: String
    var description: String?
    var timetable: String?
    var site: String?
    var phone: [String]?
    var city: String?
    var address: String?
    var lat: Double
    var lon: Double
    var photoMain: String?
    var photoAll: [String]?
    var likes: Int
    var tags: [String]?
    var distance: Int?
    
    var position: CLLocationCoordinate2D {
        return CLLocationCoordinate2D.init(latitude: lat, longitude: lon)
    }
    
    func openOnMap() {
        let coordinate = self.position
        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
        mapItem.name = self.title
        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving])
    }
    
    var mapIcon: UIImageView {
        return mapIcon(for: categoryIds.first ?? 4)
    }
    
    func mapIcon(for categoryId: Int) -> UIImageView {
        let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 37, height: 46))
        iconView.image = #imageLiteral(resourceName: "group_base.png")
        let iconViewCat = UIImageView(frame: CGRect(x: 2, y: 2, width: 33, height: 33))
        if let image =  UIImage(named: "cat_\(categoryId).png") {
            iconViewCat.image = image
        }
        iconView.addSubview(iconViewCat)
        return iconView
    }
    
    static func parse(_ json: JSON) -> Place? {
        
        guard let obj = json.dictionary else { return nil }
        guard let id = obj["id"]?.int, let categoryIdsArray = obj["category_ids"]?.array, let title = obj["title"]?.string, let lat = obj["lat"]?.double, let lon = obj["lon"]?.double, let likes = obj["likes"]?.int, let tags = obj["tags"]?.array, let phonesArray = obj["phone"]?.array else {
            print("⚠️ не удалось распарсить обязательные поля Place")
            return nil
        }
        
        return Place(
            id: id,
            categoryIds: categoryIdsArray.filter({ $0.int != nil }).map({ $0.intValue }),
            title: title,
            description: obj["description"]?.string,
            timetable: obj["timetable"]?.string,
            site: obj["site"]?.string,
            phone: phonesArray.filter({ $0.string != nil }).map({ $0.stringValue }),
            city: obj["city"]?.string,
            address: obj["address"]?.string,
            lat: lat,
            lon: lon,
            photoMain: obj["photoMain"]?.string,
            photoAll: obj["photo_all"]?.array?.filter({ $0.string != nil }).map({ $0.stringValue }),
            likes: likes,
            tags: tags.filter({ $0.string != nil }).map({ $0.stringValue }),
            distance: nil)
    }
    
}
