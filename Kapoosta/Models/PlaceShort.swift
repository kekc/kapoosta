//
//  PlaceShort.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 24/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation

struct PlaceShort {
    
    var id: Int
    var categoryIds: [Int]
    var title: String
    var lat: Double
    var lon: Double
    var likes: Int
    
}
