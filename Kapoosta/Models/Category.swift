//
//  Category.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 27/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation

struct Category {
    var id: Int
    var title: String
}
