//
//  MainViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 27/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit
import GoogleMaps

class MainViewController: RootViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var mapView: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var rhombusView: UIView!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchIconWidth: NSLayoutConstraint!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var seatchImageView: UIImageView!
    
    var isSearchActive = false {
        didSet {
            if isSearchActive {
                tableView.rowHeight = UITableViewAutomaticDimension
                tableView.estimatedRowHeight = 100
                tableView.sectionHeaderHeight = 50
                tableView.sectionFooterHeight = 30
                seatchImageView.image = #imageLiteral(resourceName: "SearchFieldEmpty.png")
                searchIconWidth.constant = 0
            } else {
                tableView.rowHeight = 114
                tableView.estimatedRowHeight = 114
                tableView.sectionHeaderHeight = 0
                tableView.sectionFooterHeight = 0
                seatchImageView.image = #imageLiteral(resourceName: "SearchField.png")
                searchIconWidth.constant = 60
                DispatchQueue.main.async {
                    self.searchTextField.resignFirstResponder()
                }
            }
            view.layoutIfNeeded()
            tableView.reloadData()
        }
    }
    
    var categories: [Category] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    var searchResult: [Place] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    override func setNavigationColors() {
        navigationController?.navigationBar.barTintColor = UIColor.white
        preferredStatusBarStyle = .default
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.sectionFooterHeight = 0
        tableView.sectionHeaderHeight = 0
        headerView.layer.cornerRadius = 19
        tableView.rowHeight = 114
        tableView.estimatedRowHeight = 114
        rhombusView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
        
        ServerManager.shared.getCategories { self.categories = $0 }
        let logoView = UIImageView(image: #imageLiteral(resourceName: "GroupClear.png"))
        logoView.contentMode = .scaleAspectFit
        logoView.frame = CGRect(x: -60.50, y: -14, width: 121, height: 28)
        navigationItem.titleView = UIView()
        navigationItem.titleView?.addSubview(logoView)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardDidShow),
            name: NSNotification.Name.UIKeyboardDidShow,
            object: nil
        )
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(keyboardWillHide),
            name: NSNotification.Name.UIKeyboardWillHide,
            object: nil
        )
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            self.bottomConstraint.constant = keyboardHeight
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        self.bottomConstraint.constant = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        tableView.frame = CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height)
        mainView.addSubview(tableView)
    }
    
    @IBAction func topButtonAction(_ sender: UIButton) {
        if sender.tag == 2 {
            let vc = storyboard?.instantiateViewController(withIdentifier: "mapVCID") as! MapViewController
            DispatchQueue.main.async {
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func configureMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 59.93, longitude: 30.32906, zoom: 6.0)
        let map = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: mainView.frame.width, height: mainView.frame.height), camera: camera)
        map.isMyLocationEnabled = true
        mapView.addSubview(map)
    }
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        mainView.backgroundColor = .white
        mainView.clipsToBounds = true
        let grayView = UIView(frame: CGRect(x: 16, y: 20, width: (UIScreen.main.bounds.width - 32), height: 54))
        grayView.backgroundColor = .backgroundGray
        grayView.layer.cornerRadius = 19
        let rhombusView = UIView(frame: CGRect(x: (UIScreen.main.bounds.width - 30) / 2, y: 5, width: 30, height: 30))
        rhombusView.backgroundColor = .backgroundGray
        rhombusView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 4))
        mainView.addSubview(rhombusView)
        mainView.addSubview(grayView)
        let label = UILabel(frame: CGRect(x: 15, y: 10, width: 150, height: 20))
        label.font = UIFont.systemFont(ofSize: 13)
        label.text = "Предложения"
        grayView.addSubview(label)
        return mainView
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 130))
        mainView.backgroundColor = .white
        mainView.clipsToBounds = true
        let grayView = UIView(frame: CGRect(x: 16, y: -30, width: (UIScreen.main.bounds.width - 32), height: 49))
        grayView.backgroundColor = .backgroundGray
        grayView.layer.cornerRadius = 19
        mainView.addSubview(grayView)
        return mainView
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isSearchActive {
            return searchResult.count
        } else {
            return categories.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isSearchActive {
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultTableViewCellID") as! SearchResultTableViewCell
            if searchResult.count > indexPath.row {
                cell.place = searchResult[indexPath.row]
            }
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryTableViewCellID") as! CategoryTableViewCell
            cell.category = categories[indexPath.row]
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isSearchActive {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "PlaceViewControllerID") as! PlaceViewController
                vc.place = self.searchResult[indexPath.row]
                self.navigationController?.pushViewController(vc, animated: true)
            }
        } else {
            DispatchQueue.main.async {
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapVCID") as! MapViewController
                vc.categoryId = self.categories[indexPath.row].id
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print(scrollView.centerYAnchor)
    }
}

extension MainViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let prevString = searchTextField.text as NSString? else { return true }
        let newString = prevString.replacingCharacters(in: range, with: string)
        searchResult = ServerManager.shared.searchPlace(patern: newString)
        isSearchActive = true
        print(newString)
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        isSearchActive = false
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        DispatchQueue.main.async {
            self.searchTextField.resignFirstResponder()
        }
        return true
    }
}
