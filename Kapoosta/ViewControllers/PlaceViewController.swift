//
//  PlaceViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 28/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit
import MapKit

class PlaceViewController: RootTableViewController {
    
    @IBOutlet weak var phoneNavigationButton: UIBarButtonItem!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var mainInfoTextView: UITextView!
    @IBOutlet weak var textViewInfoHeight: NSLayoutConstraint!
    @IBOutlet weak var distanceLabel: UILabel!
    
    var place: Place?
    var textViewSize: CGFloat = 100
    override func viewDidLoad() {
        super.viewDidLoad()
        configureVC()
    }
    
    override func setNavigationColors() {
        navigationController?.navigationBar.barTintColor = UIColor.mainGreen
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.preferredStatusBarStyle = .lightContent
    }
    
    func configureVC() {
        guard let place = self.place else { return }
        configureInfoLabel()
        let htmlText = place.description ?? ""
        if let htmlData = htmlText.data(using: String.Encoding.unicode) {
            do {
                let attributedText = try? NSAttributedString(data: htmlData, options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil)
                textView.attributedText = attributedText
            }
        }
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.dataDetectorTypes = .all
        textView.font = UIFont.systemFont(ofSize: 15)
        addressLabel.text = place.address
        let fixedWidth = view.frame.width
        let newDescriptionSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: 1e6))
        textViewSize = newDescriptionSize.height
        
        let newInfoSize = mainInfoTextView.sizeThatFits(CGSize(width: fixedWidth, height: 1e6))
        textViewInfoHeight.constant = newInfoSize.height
        distanceLabel.text = "\(PlaceDescriptionTableViewCell.distanceToString(distance: place.distance ?? 0)) \("от Вас".localized)"
        
        phoneNavigationButton.isEnabled = place.phone?.count != 0
        tableView.estimatedRowHeight = 44.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.reloadData()
    }
    
    func configureInfoLabel() {
        let text = NSMutableAttributedString()
        if let title = place?.title, title.count > 0 {
            text.append(NSAttributedString(string: "\(title)", attributes: [.font: UIFont.systemFont(ofSize: 28, weight: .bold)]))
        }
        if let tags = place?.tags?.joined(separator: ", "), tags.count > 0 {
            text.append(NSAttributedString(string: "\n", attributes: [.font: UIFont.systemFont(ofSize: 20)]))
            text.append(NSAttributedString(string: "\(tags)", attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.gray]))
        }
        if let timetable = place?.timetable, timetable.count > 0 {
            text.append(NSAttributedString(string: "\n", attributes: [.font: UIFont.systemFont(ofSize: 20)]))
            text.append(NSAttributedString(string: "● ", attributes: [.font: UIFont.systemFont(ofSize: 14) , .foregroundColor: UIColor.mainGreen]))
            text.append(NSAttributedString(string: "\(timetable)", attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.black]))
        }
        if let phones = place?.phone?.joined(separator: ", "), phones.count > 0 {
            text.append(NSAttributedString(string: "\n", attributes: [.font: UIFont.systemFont(ofSize: 20)]))
            text.append(NSAttributedString(string: "\(phones)", attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.mainGreen]))
        }
        if let site = place?.site, site.count > 0 {
            text.append(NSAttributedString(string: "\n", attributes: [.font: UIFont.systemFont(ofSize: 20)]))
            text.append(NSAttributedString(string: "Сайт: \(site)", attributes: [.font: UIFont.systemFont(ofSize: 14), .foregroundColor: UIColor.mainGreen]))
        }
        mainInfoTextView.isEditable = false
        mainInfoTextView.isScrollEnabled = false
        mainInfoTextView.dataDetectorTypes = .all
        mainInfoTextView.attributedText = text
        mainInfoTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.mainGreen]
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            return textViewSize
        }
        return UITableViewAutomaticDimension
    }
    
    @IBAction func makeRouteButtonAction(_ sender: Any) {
        place?.openOnMap()
    }
    
    @IBAction func showOnMapButtonAction(_ sender: Any) {
        guard let vc =  storyboard?.instantiateViewController(withIdentifier: "PlaceOnMapViewControllerID") as? PlaceOnMapViewController else { return }
        vc.place = place
        navigationController?.show(vc, sender: self)
    }
    
    @IBAction func shareButtonAction(_ sender: Any) {
        guard let place = self.place else { return }
        let text = "\(place.title)\n\(place.address ?? "")\n\(place.phone?.joined(separator: ", ") ?? "")"
        let textToShare = [text]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [UIActivityType.airDrop, UIActivityType.postToFacebook]
        
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    @IBAction func phoneButtonAction(_ sender: Any) {
        guard let place = self.place else { return }
        place.phone![0].makeAColl()
    }
}

extension PlaceViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return place?.photoAll?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlacePhotoID", for: indexPath) as! PlacePhotoCollectionViewCell
        cell.link = place!.photoAll![indexPath.row]
        return cell
    }
    
}
