//
//  MenuViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 23/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var leftMainConstraint: NSLayoutConstraint!
    @IBOutlet weak var rightSideButton: UIButton!
    
    var diffX: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.isToolbarHidden = true
        self.leftMainConstraint.constant = -270
        self.rightSideButton.alpha = 0
        self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.3) {
            self.leftMainConstraint.constant = 0
            self.rightSideButton.alpha = 1
            self.view.layoutIfNeeded()
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Hide the navigation bar on the this view controller
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Show the navigation bar on other view controllers
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func rightSideButtonAction(_ sender: Any) {
        popVC()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            diffX = touch.location(in: view).x
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let lastX = touch.location(in: view).x
            let nextLeft = lastX - diffX
            leftMainConstraint.constant = ((nextLeft < 0) ? nextLeft : 0)
            self.rightSideButton.alpha = 1 - (-nextLeft) / 270
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let lastX = touch.location(in: view).x
            let nextLeft = lastX - diffX
            if nextLeft < -150 {
                popVC()
            } else {
                UIView.animate(withDuration: TimeInterval(0.015 + (-nextLeft) / 150 * 0.15) ) {
                    self.rightSideButton.alpha = 1
                    self.leftMainConstraint.constant = 0
                    self.view.layoutIfNeeded()
                }
            }
        }
    }
    
    func popVC() {
        UIView.animate(withDuration: 0.3, animations: {
            self.leftMainConstraint.constant = -270
            self.rightSideButton.alpha = 0
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: false)
        }
    }

}
