//
//  ViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 23/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var downloadLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
        ServerManager.shared.initFetchPlaces {
            UIView.animate(withDuration: 0.3, animations: {
                self.startButton.backgroundColor = .mainGreen
                self.downloadLabel.alpha = 0
            })
            self.startButton.isEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        bottomViewHeightConstraint.constant = 0
        startButton.alpha = 0
        startButton.backgroundColor = .lightGray
        startButton.isEnabled = false
        self.view.layoutIfNeeded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        UIView.animate(withDuration: 0.5) {
            self.startButton.alpha = 1
        }
        UIView.animate(withDuration: 1) {
            self.bottomViewHeightConstraint.constant = 154
            self.view.layoutIfNeeded()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

