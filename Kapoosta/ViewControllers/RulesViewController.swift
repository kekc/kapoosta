//
//  RulesViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 31/07/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class RulesViewController: UIViewController, UIWebViewDelegate {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        let webUrl: NSURL = NSURL(string: "https://api.kapoosta.ru/rules/")!
        var webRequest: URLRequest = URLRequest(url: webUrl as URL)
        webRequest.addValue(Locale.current.languageCode ?? "ru", forHTTPHeaderField: "Accept-Language")
        webView.loadRequest(webRequest)
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.linkClicked {
            UIApplication.shared.open(request.url!, options: [:])
            return false
        }
        return true
    }
}
