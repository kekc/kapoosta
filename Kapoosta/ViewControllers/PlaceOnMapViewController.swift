//
//  PlaceOnMapViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 16/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils

class PlaceOnMapViewController: RootViewController {

    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var routeButton: UIButton!
    var place: Place? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let place = self.place else {
            navigationController?.popViewController(animated: true)
            return
        }
        let camera = GMSCameraPosition.camera(withLatitude: place.lat, longitude: place.lon, zoom: 10.0)
        print(self.mapView.frame)
        let map = GMSMapView.map(withFrame: self.mapView.frame, camera: camera)
        map.isMyLocationEnabled = true
        mapView.addSubview(map)
        routeButton.layer.cornerRadius = routeButton.frame.height / 2
        let marker = GMSMarker()
        marker.position = place.position
        marker.title = place.title
        marker.map = map
        marker.iconView = place.mapIcon
        self.title = "Объект на карте"
    }

    override func setNavigationColors() {
        navigationController?.navigationBar.barTintColor = UIColor.mainGreen
        preferredStatusBarStyle = .lightContent
    }
    
    @IBAction func routeButtonAction(_ sender: Any) {
        guard let place = self.place else {
            return
        }
        place.openOnMap()
    }
}
