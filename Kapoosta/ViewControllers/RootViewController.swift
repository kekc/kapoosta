//
//  RootViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 16/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    private var _preferredStyle = UIStatusBarStyle.default;
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return _preferredStyle
        }
        set {
            _preferredStyle = newValue
            self.setNeedsStatusBarAppearanceUpdate()
        }
        
    }
    
    override func willMove(toParentViewController parent: UIViewController?) {
        if let last = self.navigationController?.viewControllers.last as? RootViewController {
            if last == self && self.navigationController!.viewControllers.count > 1 {
                if let parent = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2] as? RootViewController {
                    parent.setNavigationColors()
                }
                if let parent = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2] as? RootTableViewController {
                    parent.setNavigationColors()
                }
            }
        }
        if let last = self.navigationController?.viewControllers.last as? RootTableViewController {
            if last == self && self.navigationController!.viewControllers.count > 1 {
                if let parent = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2] as? RootViewController {
                    parent.setNavigationColors()
                }
                if let parent = self.navigationController!.viewControllers[self.navigationController!.viewControllers.count - 2] as? RootTableViewController {
                    parent.setNavigationColors()
                }
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let parent = navigationController?.viewControllers.last as? RootViewController {
            parent.animateNavigationColors()
        }
        if let parent = navigationController?.viewControllers.last as? RootTableViewController {
            parent.animateNavigationColors()
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        self.setNavigationColors()
    }
    
    func animateNavigationColors(){
        transitionCoordinator?.animate(alongsideTransition: { [weak self](context) in
            self?.setNavigationColors()
            }, completion: nil)
    }
    func setNavigationColors(){
        //Override in subclasses
    }
}
