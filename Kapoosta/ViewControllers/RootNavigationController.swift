//
//  RootNavigationController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 16/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return visibleViewController!.preferredStatusBarStyle
    }
}
