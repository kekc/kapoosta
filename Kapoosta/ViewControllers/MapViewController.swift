//
//  MapViewController.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 24/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation

class MapViewController: RootViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var categoryId = 0
    var cell: MapTableViewCell? = nil
    var didFirstUpdate = false
    var isDragging = false
    var locationManager = CLLocationManager()
    
    var places: [Place] = [] {
        didSet {
            if !didFirstUpdate {
                cell?.updatePlaces(places)
                didFirstUpdate = true
            }
            tableView.reloadData()
        }
    }
    
    override func setNavigationColors() {
        navigationController?.navigationBar.barTintColor = UIColor.mainGreen
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.preferredStatusBarStyle = .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ServerManager.shared.getCategory(by: categoryId) { (category) in
            if category == nil {
                self.navigationItem.title = "Всё места"
            } else {
                self.navigationItem.title = category?.title
            }
        }
    }
    
}

extension MapViewController: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        ServerManager.shared.getPlaces(target: position.target, categoryId: categoryId) { self.places = $0 }
    }
}

extension MapViewController: UITableViewDelegate, UITableViewDataSource {
    
    @objc func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        let yOffset = scrollView.contentOffset.y
        switch yOffset {
        case 0...200:
            tableView.setContentOffset(.zero, animated: true)
        case 200...350:
            tableView.setContentOffset(CGPoint(x: 0, y: 350), animated: true)
        default: break
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MapTableViewCellID") as! MapTableViewCell
            if cell.delegate == nil {
                cell.delegate = self
            }
            cell.categoryId = categoryId
            self.cell = cell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "PlaceDescriptionTableViewCellID") as! PlaceDescriptionTableViewCell
            cell.place = places[indexPath.row - 1]
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
        perform(#selector(self.scrollViewDidEndScrollingAnimation(_:)), with: scrollView, afterDelay: 0.1)
        if scrollView.contentOffset.y < 0 {
            scrollView.contentOffset.y = 0
        } else {
            cell?.updateHeight(with: scrollView.contentOffset.y)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard places.count > (indexPath.row - 1) else { return }
        let vc = storyboard?.instantiateViewController(withIdentifier: "PlaceViewControllerID") as! PlaceViewController
        vc.place = places[indexPath.row - 1]
        DispatchQueue.main.async {
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    
}
