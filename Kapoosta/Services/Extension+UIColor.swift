//
//  Extension+UIColor.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 16/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    static let mainGreen = UIColor(red: 39/255, green: 174/255, blue: 96/255, alpha: 1)
    static let textGray = UIColor(red: 230/255, green: 206/255, blue: 206/255, alpha: 1)
    static let backgroundGray = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
}
