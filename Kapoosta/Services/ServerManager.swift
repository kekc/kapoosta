//
//  ServerManager.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 24/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import GoogleMaps

class ServerManager {
    private let placesUDKey = "placesUDKey"
    static let shared = ServerManager()
    
    private func parseURL(_ response: DefaultDataResponse) -> String {
        return response.request?.url?.absoluteString ?? "Ошибка адреса"
    }
    
    private func parseJSON(_ response: DefaultDataResponse) -> JSON? {

        guard response.error == nil else {
            print("⚠️ запрос выполнен с ошибкой \(response.error!.localizedDescription) (\(parseURL(response)) ")
            return nil
        }
        guard response.response?.statusCode == 200 else {
            print("⚠️ запрос выполнен с кодом \(response.response?.statusCode ?? -1) (\(parseURL(response)))")
            return nil
        }
        guard let data = response.data else {
            print("⚠️ data == nil (\(parseURL(response)))")
            return nil
        }
        print("✅ загружено \(data) (\(parseURL(response)))")
        return JSON(data)

    }
    
    fileprivate var savedResult: [Place] = []
    
    func searchPlace(patern: String) -> [Place] {
        var result = savedResult.filter { (place) -> Bool in
            return place.title.contains(patern)
        }
        return result
    }
    
    func getPlaces(target: CLLocationCoordinate2D? = nil, categoryId: Int = 0, callback: (([Place]) -> Void)? = nil) {
        var result = savedResult
        if categoryId != 0 {
            result = result.filter({ $0.categoryIds.contains(categoryId) })
        }
        if let target = target {
            result = result.map { (place: Place) -> Place in
                let point1 = CLLocationCoordinate2D.init(latitude: place.lat, longitude: place.lon)
                var placeValue = place
                placeValue.distance = Int(CLLocation.distance(from: point1, to: target))
                return placeValue
            }.sorted(by: { $0.distance ?? 1000000000 < $1.distance ?? 1000000000 })
        }
        callback?(result)
    }
    
    let headers: HTTPHeaders = [
        "Accept-Language": Locale.current.languageCode ?? "ru",
    ]
    
    func initFetchPlaces(callback: @escaping (() -> Void)) {
        if let data = UserDefaults.standard.value(forKey: placesUDKey) as? Data {
            let places = try? PropertyListDecoder().decode(Array<Place>.self, from: data)
            if let places = places, places.count > 0 {
                callback()
                self.savedResult = places
                print("🌝 Достал из кеша")
            } else {
                print("🌝 Не смог достать из кеша")
            }
        }
        
        Alamofire.request("https://api.kapoosta.ru/api/v1/places/", headers: headers).response { (response) in
            guard let json = self.parseJSON(response) else { return }
            guard let arr = json.array else {
                print("⚠️ не удалось получить массив api/v1/places")
                return
            }
            //https://api.kapoosta.ru/   http://evarand.rocks/kapoosta/api/v1/places
            callback()
            self.savedResult = arr.map({ Place.parse($0) }).filter({ $0 != nil }).map({ $0! })
            UserDefaults.standard.set(try? PropertyListEncoder().encode(self.savedResult), forKey: self.placesUDKey)

            print("🌝 Сохранил в кеш")
        }
    }
    
    func getCategories(callback: @escaping (([Category]) -> Void)) {
        callback(categories)
    }
    
    func getCategory(by id: Int, callback: @escaping ((Category?) -> Void)) {
        callback(categories.first(where: { $0.id == id }))
    }
    
    private let categories = [
        Category(id: 1, title: "Мероприятия".localized),
        Category(id: 2, title: "Нужны волонтёры".localized),
        Category(id: 3, title: "Организации".localized),
        Category(id: 4, title: "Магазины".localized),
        Category(id: 5, title: "Эксперты".localized),
        Category(id: 6, title: "Кафе".localized),
        Category(id: 7, title: "Зеленые маршруты".localized),
        Category(id: 8, title: "Сбор мусора".localized)]

}


