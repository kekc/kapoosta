//
//  Extension+CLLocation.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 30/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import GoogleMaps

extension CLLocation {
    
    /// Get distance between two points
    ///
    /// - Parameters:
    ///   - from: first point
    ///   - to: second point
    /// - Returns: the distance in meters
    static func distance(from: CLLocationCoordinate2D, to: CLLocationCoordinate2D) -> CLLocationDistance {
        let from = CLLocation(latitude: from.latitude, longitude: from.longitude)
        let to = CLLocation(latitude: to.latitude, longitude: to.longitude)
        return from.distance(from: to)
    }
}
