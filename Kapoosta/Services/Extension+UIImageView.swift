//
//  Extension+UIImageView.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 28/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    
    func downloadedFrom(url: URL, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        contentMode = mode
        URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() {
                self.image = image
            }
            }.resume()
    }
    
    func downloadedFrom(link: String, contentMode mode: UIViewContentMode = .scaleAspectFit) {
        guard let url = URL(string: link.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!) else {
            print("ITDBG: не удалось распарсить ссылку")
            return
        }
        print("ITDBG: \(url)")
        downloadedFrom(url: url, contentMode: mode)
    }
    
}
