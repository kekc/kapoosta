//
//  PlaceDescriptionTableViewCell.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 25/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class PlaceDescriptionTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var distantionLabel: UILabel!
    
    var place: Place? = nil {
        didSet {
            guard let placeValue = place else { return }
            titleLabel.text = placeValue.title
            infoLabel.text = placeValue.tags?.joined(separator: ", ")
            likesLabel.text = "\(placeValue.likes) \("лайков".localized)"
            distantionLabel.text = PlaceDescriptionTableViewCell.distanceToString(distance: placeValue.distance ?? 0)
        }
    }
    
    static func distanceToString(distance: Int) -> String {
        if distance < 5 {
            return "Рядом".localized
        } else if distance < 100 {
            return "\(distance) \("м".localized)"
        } else if distance < 1000 {
            return "\((distance / 10) * 10) \("м".localized)"
        } else if distance < 1000000 {
            return "\(distance / 1000) \("км".localized)"
        } else {
            return ""
        }
    }
}
