//
//  MapTableViewCell.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 24/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit
import GoogleMaps
import GoogleMapsUtils

protocol MapCellDelegate {
    func updateHeight(with offset: CGFloat)
    func updatePlaces(_ places: [Place])
}

class MapTableViewCell: UITableViewCell, MapCellDelegate, GMUClusterManagerDelegate, GMSMapViewDelegate {

    let MAP_HEIGHT: CGFloat = 350
    
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var mapViewHeight: NSLayoutConstraint!
    private var clusterManager: GMUClusterManager!
    var button = UIButton()
    var locationManager = CLLocationManager()
    var categoryId = 4
    var delegate: GMSMapViewDelegate? = nil {
        didSet {
            map?.delegate = delegate
        }
    }
    var map: GMSMapView? = nil
    var prevLocation: CLLocationCoordinate2D? = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        let camera = GMSCameraPosition.camera(withLatitude: 59.93, longitude: 30.32906, zoom: 10.0)
        let map = GMSMapView.map(withFrame: CGRect(x: 0, y: 0, width: Int(UIScreen.main.bounds.width), height: Int(MAP_HEIGHT)), camera: camera)
        map.isMyLocationEnabled = true
        self.map = map
        mapView.addSubview(map)
        let iconGenerator = MapClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: map,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: map, algorithm: algorithm,
                                           renderer: renderer)
        clusterManager.setDelegate(self, mapDelegate: self)
        clusterManager.cluster()
        
        let button = UIButton(frame: CGRect(x: UIScreen.main.bounds.width - 52, y: 340 - 42, width: 36, height: 36))
        button.setImage(#imageLiteral(resourceName: "NearMe"), for: .normal)
        button.alpha = 0.9
        button.addTarget(self, action: #selector(MapTableViewCell.locationButtonTap(_:)), for: UIControlEvents.touchDown)
        
        mapView.addSubview(button)
        
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        
    }
    
    @objc func locationButtonTap(_ sender: Any) {
        guard let prevLocation = self.prevLocation else { return }
        map?.animate(toLocation: prevLocation)
    }
    
    func updateHeight(with offset: CGFloat) {
        mapViewHeight.constant = MAP_HEIGHT - offset / 2
        layoutIfNeeded()
    }
    
    func updatePlaces(_ places: [Place]) {
        clusterManager.clearItems()
        places.forEach { (place) in
            clusterManager.add(PlaceClusterItem(place: place))
        }
        clusterManager.cluster()
    }
}

class MapClusterIconGenerator: GMUDefaultClusterIconGenerator {
    
    override func icon(forSize size: UInt) -> UIImage {
        let iconView = UIImageView(frame: CGRect(x: 0, y: 0, width: 37, height: 46))
        iconView.image = #imageLiteral(resourceName: "icon_map_cluster.png")
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 37, height: 37))
        label.text = size < 100 ? "\(size)" : "9+"
        label.textAlignment = .center
        label.textColor = UIColor(red: 0.153, green: 0.682, blue: 0.376, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 14, weight: .bold)
        iconView.addSubview(label)
        return image(with: iconView)!
    }
    
    private func image(with view: UIView) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0.0)
        defer { UIGraphicsEndImageContext() }
        if let context = UIGraphicsGetCurrentContext() {
            view.layer.render(in: context)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            return image
        }
        return nil
    }
    
}

extension MapTableViewCell: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locValue: CLLocationCoordinate2D = manager.location!.coordinate
        if prevLocation == nil {
            map?.camera = GMSCameraPosition.camera(withTarget: locValue, zoom: map?.camera.zoom ?? 10)
            button.alpha = 0.9
        }
        prevLocation = locValue
    }
}

extension MapTableViewCell: GMUClusterRendererDelegate {
    func renderer(_ renderer: GMUClusterRenderer, markerFor object: Any) -> GMSMarker? {
        let marker = GMSMarker()
        if let place = object as? PlaceClusterItem {
            marker.iconView = place.place.mapIcon(for: categoryId)
            marker.title = place.place.title
        }
        return marker
    }
}
