//
//  PlacePhotoCollectionViewCell.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 28/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class PlacePhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var iamgeView: UIImageView!
    
    var link: String? {
        didSet {
            print("ITDBG: \(link)")
            guard let linkValue = link else { return }
            print("ITDBG: \(linkValue)")
            iamgeView.downloadedFrom(link: linkValue, contentMode: .scaleToFill)
        }
    }
    
}
