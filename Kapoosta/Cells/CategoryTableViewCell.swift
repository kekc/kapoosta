//
//  CategoryTableViewCell.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 27/05/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var category: Category! {
        didSet {
            mainImageView.image = UIImage(named: "cat_\(category.id).png")
            titleLabel.text = category.title 
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mainView.layer.cornerRadius = 8
        mainImageView.layer.cornerRadius = mainImageView.frame.width / 2
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
