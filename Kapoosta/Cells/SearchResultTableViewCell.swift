//
//  SearchResultTableViewCell.swift
//  Kapoosta
//
//  Created by Ivan Trofimov on 07/06/2018.
//  Copyright © 2018 Ivan Trofimov. All rights reserved.
//

import UIKit

class SearchResultTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabelString: UILabel!
    
    var place: Place? {
        didSet {
            guard let place = self.place else { return }
            let text = NSMutableAttributedString()
            text.append(NSAttributedString(string: "\(place.title) \("Hi".localized)", attributes: [
                .font: UIFont.systemFont(ofSize: 24),
                .foregroundColor: UIColor.mainGreen
                ]))
            if let tags = place.tags?.joined(separator: ", ") {
                text.append(NSAttributedString(string: "\n\(tags)", attributes: [
                    .font: UIFont.systemFont(ofSize: 13),
                    .foregroundColor: UIColor.textGray
                    ]))
            }
            titleLabelString.attributedText = text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

}
